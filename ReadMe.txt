https://github.com/typicode/json-server


##### Install JSON Server
--------------------------
npm install -g json-server


##### Invoke Json Server with DB.json
---------------------------------------
json-server --watch db.json (http://localhost:3000/)


##### Create db.json file where Jsonserver is installed
--------------------------------------------------
for ex. C:\Users\User


##### JSON Dummy File
-------------------

{
	"posts": [{
			"id": 1,
			"title": "Selenium with c#",
			"author": "Sushant"
		},
		{
			"id": 2,
			"title": "RestAssured API",
			"author": "Sushant"
		},
		{
			"id": 3,
			"title": "Selenium with Java",
			"author": "Sushant"
		}
	],
	"comments": [{
			"Id": 1,
			"Body": "Excellent",
			"PostId": 1
		},
		{
			"Id": 2,
			"Body": "Very Good",
			"PostId": 2
		}
	],
	"profile": {
		"Name": "Anaya"
	},
	"address": [{
		"Primary": [{
				"City": "NewCastle",
				"State": "Tyne and wear",
				"Country": "United Kingdom",
				"Postcode": "Ne270ut"
			},
			{
				"City": "Leeds",
				"State": "Yorkshire",
				"Country": "United Kingdom",
				"Postcode": "Ne128nh"
			}
		],
		"Secondary": [{
				"City": "London",
				"State": "London",
				"Country": "United Kingdom",
				"Postcode": "Ls270ut"
			},
			{
				"City": "Manchestor",
				"State": "Manchestor",
				"Country": "United Kingdom",
				"Postcode": "Ma128nh"
			}
		]
	}]
}