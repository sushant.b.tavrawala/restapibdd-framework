https://github.com/typicode/json-server


##### Install JSON Server
--------------------------
npm install -g json-server


##### Invoke Json Server with DB.json
---------------------------------------
json-server --watch db.json (http://localhost:3000/)


##### Create db.json file where Jsonserver is installed
--------------------------------------------------
for ex. if Json server is installed in C:\Users\User


##### JSON Dummy File
----------------------
src/test/resources/testData/testData.json
