Feature: Validate the Books Post Request

  Background:
    Given Service is running with path "/posts/"

  Scenario Outline: 1- POST request to add the product
    And user add id <id>
    And user add author "<author>"
    And user add title "<title>"
    Then Service call with POST Method
    And validate the response with "<title>"
    And validate the response with statuscode <statuscode>

    Examples:
      | id | author | title     | statuscode |
      | 7  | ABC    | SBT-TITLE | 201        |
