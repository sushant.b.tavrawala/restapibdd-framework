Feature: Validate the Address Details

  Background:
    Given Service is running with path "/address/"

  Scenario: 1- GET request to validate city
    Then Service call with GET Method
    And validate the response with statuscode 200
    And capture the address city is "London"

  Scenario: 2- GET request to validate city with deseralisation
    Then Service call with GET Method
    And capture dynamic json response
