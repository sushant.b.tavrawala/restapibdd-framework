package Steps;

import Helper.*;

import Helper.AddressBody.Address;
import Helper.AddressBody.AddressAry;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StringArrayDeserializer;
import com.sun.xml.internal.fastinfoset.util.StringArray;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import org.assertj.core.api.Assertions;
import org.testng.Assert;

import javax.swing.plaf.basic.BasicListUI;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Steps {

    private Config config = new Config();
    private PostBody postBody = new PostBody();
    private ApiCallResponse response = new ApiCallResponse();
    public String responsePrimaryCity;
    public String responseSecondaryCity;
    JsonPath json;

    @Given("Service is running with path {string}")
    public void serviceIsRunningWithPath(String path) {
        RestAssured.baseURI= config.startUrl(path);
    }

    @And("user add id {int}")
    public void userAddIdId(int id) {
        postBody.setId(id);
    }

    @And("user add author {string}")
    public void userAddAuthor(String author) {
        postBody.setAuthor(author);
    }

    @And("user add title {string}")
    public void userAddTitle(String title) {
        postBody.setTitle(title);
    }

    @And("validate the response with {string}")
    public void validateTheResponseWith(String expectedTitle) {
        json = new JsonPath(response.getResponse().asString());
        String responseTitle = json.getString("title");

        Assert.assertEquals(responseTitle,expectedTitle);
    }

    @And("validate the response with statuscode {int}")
    public void validateTheResponseWithStatuscodeStatuscode(int statuscode) {
        Assert.assertEquals(response.getResponse().statusCode(),statuscode);
    }

    @And("capture the address city is {string}")
    public void captureThePrimaryAddressCityIs(String city) throws JsonProcessingException {

      Address[] addressList = new ObjectMapper().readValue(response.getResponse().asString(), Address[].class);

      //Get addresses Array from the list
      for(Address responseAddress : addressList)
      {
        //Get primary Address
        for(AddressAry checkPrimaryAdd: responseAddress.getPrimary())
        {
            responsePrimaryCity = checkPrimaryAdd.getCity();
           if(responsePrimaryCity.equalsIgnoreCase(city))
           {
               Assertions.assertThat(responsePrimaryCity).isEqualTo(city);
               System.out.println(responsePrimaryCity + " is found in Primary Address");
               break;
           }
        //Get secondary address
            for(AddressAry checkSecondaryAdd:responseAddress.getSecondary())
            {
                responseSecondaryCity = checkSecondaryAdd.getCity();
               if (responseSecondaryCity.equalsIgnoreCase(city))
               {
                   Assertions.assertThat(responseSecondaryCity).isEqualTo(city);
                   System.out.println(responseSecondaryCity + " is found in Secondary Address");
                   break;
               } else if(responsePrimaryCity != city || responseSecondaryCity != city)
               {
                   Assert.fail(city +" is not found in the list");
               }
            }
        }
     }
   }

    @And("capture dynamic json response")
    public void captureDynamicJsonResponse() {

        Object responseAsObject = response.getResponse().as(Object.class);
        if (responseAsObject instanceof List) {
            List responseAsList = (List) responseAsObject;
            System.out.println("List Object Size: " + responseAsList.size());
            System.out.println(responseAsList);
        } else if (responseAsObject instanceof Map) {
            Map responseAsMap = (Map) responseAsObject;
            System.out.println("Map Object Size: " + responseAsMap.keySet());
        }

    }


    //Calling POST Method
    @Then("Service call with POST Method")
    public void serviceCallWithPOSTMethod() {
        response.setResponse(RestAssured.given()
                .log().all()
                .headers(config.get_Headers())
                .body(postBody)
                .when()
                .post()
                .then()
                .log().all().extract().response());

        /*
        response = RestAssured.given()
                .log().all()
                .headers(startConfig.get_Headers())
                .body(postBody)
                .when()
                .post()
                .then()
                .log().all().extract().response();

         */
    }

    //Calling GET Method
    @Then("Service call with GET Method")
    public void service_call_with_get_method() {
        response.setResponse(RestAssured.given()
                .log().all()
                .headers(config.get_Headers())
                .when()
                .get()
                .then()
                .log().all().extract().response());
    }



}
