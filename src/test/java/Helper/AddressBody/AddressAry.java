package Helper.AddressBody;


public class AddressAry {

    private String city;
    private String state;
    private String country;
    private String postcode;

    public String getCity() { return city; }

    public String getState() { return state; }

    public String getCountry() { return country; }

    public String getPostcode() { return postcode; }
}
