package Helper;

import io.restassured.response.Response;

public class ApiCallResponse {

    private Response response;

    public Response getResponse(){
        return response;
    }

    public void setResponse(Response response){
        this.response = response;
    }
}
