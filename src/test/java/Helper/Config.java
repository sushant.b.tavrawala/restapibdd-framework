package Helper;

import io.restassured.http.Header;
import io.restassured.http.Headers;

public class Config {

    public String startUrl(String endpoint){
        return System.getProperty("baseUrl","http://localhost:3000"+endpoint);
    }

    public Headers get_Headers() {
        Header contentType = new Header("Content-Type", "application/json");
        Header contentControl = new Header("Content-Control", "no-cache");
        // Header services = new Header("services", service);
        return new Headers(contentType, contentControl);
    }

}
